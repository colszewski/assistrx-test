<?php namespace App\Http\Controllers;

use App\Patient;
use App\Song;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PatientController extends Controller
{

    /**
     * Fetch all of the patients, ordered by first name, last name ascending
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @return Response
     */
    public function index()
    {
        $patients = Patient::orderBy('patient_name')->get();
        return view('patient.index', ['patients' => $patients, 'title' => 'Patient Selection Page']);
    }

    /**
     * Return the view to modify the patients favorite song
     * If they already have one selected, send its song data
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @param int $id Patient ID
     * @return Response
     */
    public function edit($id)
    {
        $patient = Patient::find($id);
        if ($patient === null) {
            return redirect('/patient')->with('error', 'Invalid patient specified.');
        }

        // Pass the song data for the patient's favorite song, if they have one
        $song = $patient->favoriteSong;
        $songData = ($song !== null) ? json_decode($song['song_data']) : null;

        $viewData = [
            'patient' => $patient,
            'songData' => $songData,
            'title' => 'Patient Song Selection'
        ];

        return view('patient.edit', $viewData);
    }

    /**
     * Update a patient's favorite song
     *
     * If the patient's previously selected favorite is no longer used by any patients, it will be automatically deleted
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @param Request $request
     * @param int $id Patient ID
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $patient = Patient::find($id);
        if ($patient === null) {
            return redirect('/patient')->with('error', 'Invalid patient specified.');
        }

        $rawSongData = $request->input('rawSongData');

        // Being passed null or an empty string will clear the patients favorite song
        if ($rawSongData === null || $rawSongData === '') {
            $patient->favorite_song_id = null;
            $patient->save();

            // In this case, redirect the user back to the overview page since they wanted to clear the patient's song
            return redirect("/patient")->with('success', "Removed $patient->patient_name's' favorite song.");
        }

        // Check to see if the song already exists in the database
        $songHash = Song::getSongHash($rawSongData);
        $existingSong = Song::where('song_hash', '=', $songHash)->first();
        if ($existingSong !== null) {
            // If it does already exist, simply use the existing song ID
            $patient->favorite_song_id = $existingSong->song_id;
        } else {
            // If it does not already exist, create the new song and use the generated song ID
            $songData = json_decode($rawSongData);

            $song = Song::create([
                'song_name' => $songData->trackName,
                'song_artist' => $songData->artistName,
                'song_data' => $rawSongData
            ]);

            $patient->favorite_song_id = $song->song_id;
        }

        // Save our updated patient model and reload the page to show the updated artwork and song preview
        $patient->save();
        return redirect("/patient/$id/edit")->with('success', "Updated $patient->patient_name's favorite song.");
    }
}
