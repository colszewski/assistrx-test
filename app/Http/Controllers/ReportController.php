<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Patient;
use App\Song;

class ReportController extends Controller
{

    /**
     * Returns a view with all patients and metrics based on the popularity of songs
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @return Response
     */
    public function index()
    {
        $songMetrics = Song::getPopularityMetrics();

        $patients = Patient::with('favoriteSong')->orderBy('patient_name')->get();

        $viewData = [
            'patients' => $patients,
            'title' => 'Report Page',
            'background' => 'rainbow-background',
            'songMetrics' => $songMetrics
        ];

        return view('report.index', $viewData);
    }

}
