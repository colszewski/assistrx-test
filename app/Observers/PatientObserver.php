<?php namespace App\Observers;

use App\Patient;
use App\Song;

class PatientObserver
{

    /**
     * Whenever a patient is saved, check to see if their favorite song was modified
     * If it was and no other patients reference the song, delete it
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @param Patient $model
     */
    public function saved(Patient $model)
    {
        $originalFavoriteSongId = $model->getOriginal('favorite_song_id');
        if ($originalFavoriteSongId === null) {
            return;
        }

        $similarPatientCount = Patient::where('favorite_song_id', '=', $originalFavoriteSongId)->count();
        if ($similarPatientCount === 0) {
            Song::destroy($originalFavoriteSongId);
        }
    }

}
