<?php namespace App;

use App\Observers\PatientObserver;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{

    /**
     * Use `patient_id` instead of the Eloquent default `id` to match the original table
     *
     * @var string
     */
    protected $primaryKey = 'patient_id';

    /**
     * Allow all fields to be mass-assigned except for the patient_id which will be generated
     *
     * @var array
     */
    protected $guarded = ['patient_id'];

    /**
     * Register our PatientObserver class
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     */
    public static function boot()
    {
        parent::boot();

        Patient::observe(new PatientObserver);
    }

    /**
     * Returns a patient's favorite song object if one is specified, otherwise returns null
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function favoriteSong()
    {
        return $this->belongsTo('App\Song', 'favorite_song_id', 'song_id');
    }

    /**
     * Simple helper function to indicate whether or not a patient has a favorite song specified
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @return bool
     */
    public function hasFavoriteSong()
    {
        return $this->favorite_song_id !== null;
    }
}
