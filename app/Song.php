<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Song extends Model
{
    
    /**
     * Use `song_id` instead of the Eloquent default `id` to match the original table
     *
     * @var string
     */
    protected $primaryKey = 'song_id';

    /**
     * Do not allow the song's ID or hash to be mass-assigned
     * The ID will be automatically assigned and the song hash will be created from the song's data
     *
     * @var array
     */
    protected $guarded = ['song_id', 'song_hash'];

    /**
     * Get all of the patients that specified this song as their favorite
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function patients()
    {
        return $this->hasMany('App\Patient', 'favorite_song_id');
    }

    /**
     * Update the song's hash whenever the song's data is modified
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @param string $value JSON formatted song data
     */
    public function setSongDataAttribute($value)
    {
        $this->attributes['song_data'] = $value;
        $this->attributes['song_hash'] = self::getSongHash($value);
    }

    /**
     * Abstracted here so that the hashing algorithm can be easily modified
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @param string $songData JSON formatted song data
     * @return string MD5 hash of song data
     */
    public static function getSongHash($songData)
    {
        return md5($songData);
    }

    /**
     * Return a set of metrics based on the popularity of songs saved in the database
     *
     * @author Chris Olszewski <colszewski@outlook.com>
     * @since 11/20/14
     * @param int $limit The top x entries for each field
     * @return array
     */
    public static function getPopularityMetrics($limit = 5)
    {
        // All of the fields we are going to be tracking
        $metrics = [
            'artist' => [],
            'collection' => [],
            'track' => [],
            'genre' => []
        ];

        // Since we are working with arrays this creates a new copy of the metrics array structure and not a reference
        $rawMetrics = $metrics;

        // Instead of tracking by names with possible duplicates, track by unique ID and attach the name at the end
        $crossReferences = [];

        // Loop through all songs and eager load the patients who have selected these songs
        $songs = self::with('patients')->get();
        foreach ($songs as $song) {
            $patientCount = $song->patients->count();

            $songData = json_decode($song->song_data);
            foreach ($rawMetrics as $key => &$rawMetric) {
                // The fields we are using follow a standard naming convention with the exception of genre
                $dataKey = ($key !== 'genre') ? $key . 'Id' : 'primaryGenreName';

                // In testing, certain cases occurred where a genre was missing from the song's data
                if (!isset($songData->$dataKey)) {
                    continue;
                }

                // The ID of the current attribute for the current song
                $dataValue = $songData->$dataKey;

                // If this is the first time we have come across this song, be nice and initialize it
                if (!isset($rawMetric[$dataValue])) {
                    $rawMetric[$dataValue] = 0;
                }

                // Increment the counter for this ID by the number of patients who selected this song
                $rawMetric[$dataValue] += $patientCount;

                // Save a cross-reference so that we can associate IDs to names below
                $dataName = ($key !== 'genre') ? $key . 'Name' : 'primaryGenreName';
                $crossReferences[$key][$dataValue] = $songData->$dataName;
            }

            // Because we use the reference &$rawMetric, if we do not unset it we can experience some nasty behavior
            unset($rawMetric);
        }

        foreach ($rawMetrics as $key => &$rawMetric) {
            // Reverse sort each field by the number of patients with a given ID
            arsort($rawMetric);

            $fieldRowCount = 0;

            // Use our cross-reference table from earlier to create a standardized result set
            foreach ($rawMetric as $id => $count) {
                // Ensure we only return up to the limit specified
                if($fieldRowCount++ >= $limit) {
                    break;
                }

                $metrics[$key][] = [
                    'id' => $id,
                    'name' => $crossReferences[$key][$id],
                    'count' => $count
                ];
            }
        }

        return $metrics;
    }
}
