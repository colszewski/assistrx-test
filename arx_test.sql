CREATE DATABASE  IF NOT EXISTS `arx_test` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `arx_test`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: arx_test
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_11_18_102111_create_songs_table',1),('2014_11_18_102245_create_patients_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `patients` (
  `patient_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `patient_phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `patient_age` smallint(5) unsigned NOT NULL,
  `favorite_song_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`patient_id`),
  KEY `patients_favorite_song_id_foreign` (`favorite_song_id`),
  CONSTRAINT `patients_favorite_song_id_foreign` FOREIGN KEY (`favorite_song_id`) REFERENCES `songs` (`song_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES (1,'Kamal Small','532-498-9040',5,11,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(2,'Gray Stein','117-694-4940',5,NULL,'2014-11-19 17:27:55','2014-11-19 20:29:42'),(3,'Elijah Galloway','507-183-4163',1,NULL,'2014-11-19 17:27:55','2014-11-18 22:59:47'),(4,'Lev Gutierrez','574-854-0319',74,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(5,'Keegan Terry','259-583-4643',100,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(6,'Reece Lambert','615-359-9985',42,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(7,'Tad Stevens','586-177-6625',9,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(8,'Melvin Duffy','502-358-4019',4,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(9,'Burke Washington','863-758-5193',93,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(10,'Lucian Baker','221-846-8793',61,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(11,'Fritz Horton','834-933-6601',57,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(12,'Murphy Greer','502-204-3128',80,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(13,'Armando Maddox','741-189-4387',73,NULL,'2014-11-19 17:27:55','2014-11-20 02:47:45'),(14,'Isaac Hickman','869-505-5366',74,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(15,'Avram Sanchez','996-142-4633',59,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(16,'Rahim Potts','554-992-6976',45,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(17,'Ethan Weeks','296-276-1366',69,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(18,'Buckminster Evans','158-823-3623',98,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(19,'Simon Donovan','746-148-0108',80,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(20,'Chase Deleon','589-619-1973',29,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(21,'Cade Beasley','540-477-8483',25,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(22,'Wayne Jordan','382-507-3934',92,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(23,'Hiram Hartman','786-882-3860',69,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(24,'Jackson Howard','686-539-3543',33,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(25,'Richard Gonzalez','722-269-9388',54,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(26,'Cairo Patton','213-669-4347',33,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(27,'Samson Hahn','826-653-8845',83,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(28,'Harlan Shields','623-895-0316',32,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(29,'Lawrence Albert','833-671-8474',84,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(30,'Marsden Gardner','311-708-0469',92,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(31,'Macon Frederick','136-734-0618',82,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(32,'Aaron Gallagher','641-384-4924',77,NULL,'2014-11-19 17:27:55','2014-11-20 02:47:36'),(33,'Dalton Thornton','428-158-3157',61,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(34,'Lionel Gregory','741-787-1909',45,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(35,'Jason Watkins','707-991-1716',61,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(36,'Joshua Shelton','224-861-2780',57,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(37,'Aquila Baxter','605-291-1710',84,NULL,'2014-11-19 17:27:55','2014-11-20 02:47:42'),(38,'Drake Richards','193-585-7233',42,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(39,'Lawrence Cline','377-473-3253',45,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(40,'Lyle Bright','669-949-8846',37,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(41,'Darius Weiss','763-386-0597',97,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(42,'Galvin Alvarado','574-280-8892',74,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(43,'Reuben Bruce','254-541-1849',49,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(44,'Ishmael Mayo','525-820-6714',78,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(45,'Wayne Floyd','720-532-0875',99,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(46,'Stewart Gill','588-943-2914',100,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(47,'Raja Combs','229-156-3626',87,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(48,'Dale King','674-509-3997',23,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(49,'Brady Pickett','457-278-4790',43,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(50,'Denton Nunez','695-941-6967',91,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(51,'Ulysses Munoz','868-942-5143',98,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(52,'Wallace Bates','904-261-1206',37,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(53,'Luke Mccall','934-903-5929',21,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(54,'Preston Bryan','700-610-6623',73,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(55,'Garth Marquez','709-459-7087',40,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(56,'Garrett Patterson','639-445-4526',59,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(57,'Slade Delacruz','819-276-2608',60,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(58,'Ray Tanner','452-839-1236',81,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(59,'Scott Conrad','147-875-0678',34,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(60,'Yoshio Larson','471-265-7920',90,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(61,'Hashim Foster','301-121-5002',63,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(62,'Bernard Walls','492-446-1588',9,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(63,'Lucian Schmidt','477-809-8101',28,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(64,'Lionel Cook','398-482-3158',80,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(65,'Philip Harding','664-348-0160',14,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(66,'Kareem Copeland','274-897-7715',95,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(67,'Josiah Petersen','535-371-9243',7,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(68,'Isaac Little','756-378-0279',2,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(69,'Henry Moses','205-771-1248',45,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(70,'Garrison Berg','971-428-9916',84,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(71,'Caldwell Tyson','509-971-0388',48,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(72,'Abbot Bradford','985-394-0083',92,NULL,'2014-11-19 17:27:55','2014-11-20 02:47:39'),(73,'Porter Gilliam','141-337-3270',44,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(74,'Paul Greer','358-367-1200',65,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(75,'Cade Carpenter','550-381-7030',77,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(76,'Marsden Guzman','578-301-8944',67,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(77,'Louis Norman','773-988-4902',8,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(78,'Preston Gamble','480-125-7088',17,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(79,'Logan Richard','614-371-0108',92,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(80,'Howard Sharp','979-703-6148',75,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(81,'Colt Stout','600-286-3291',46,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(82,'Wade Crawford','581-961-7587',92,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(83,'Hyatt Watts','436-612-6245',92,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(84,'Price Santana','882-807-6273',33,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(85,'Honorato Thomas','228-611-9871',16,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(86,'George Herman','693-471-2093',78,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(87,'Lucius Mcconnell','599-980-2674',83,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(88,'Neville Garcia','684-920-8646',82,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(89,'Phelan Merrill','813-579-4692',71,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(90,'Brody Wiggins','557-283-0333',47,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(91,'Castor Wilkins','977-746-8805',74,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(92,'Jameson Collins','688-773-3552',92,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(93,'Kasimir Mendez','290-480-1676',29,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(94,'Lars Hodges','411-837-6254',62,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(95,'Tyrone York','219-771-5854',91,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(96,'Odysseus Carson','634-377-4968',74,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(97,'Elliott Bradford','377-378-3651',80,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(98,'Brennan Barker','469-176-4181',99,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(99,'Emmanuel Bush','618-921-7944',80,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55'),(100,'Ralph Cannon','399-502-0938',56,NULL,'2014-11-19 17:27:55','2014-11-19 17:27:55');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `songs` (
  `song_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `song_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `song_artist` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `song_data` text COLLATE utf8_unicode_ci NOT NULL,
  `song_hash` varchar(32) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`song_id`),
  KEY `song_hash_index` (`song_hash`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs`
--

LOCK TABLES `songs` WRITE;
/*!40000 ALTER TABLE `songs` DISABLE KEYS */;
INSERT INTO `songs` VALUES (11,'I Knew You Were Trouble','Taylor Swift','{\"wrapperType\":\"track\",\"kind\":\"song\",\"artistId\":159260351,\"collectionId\":571445253,\"trackId\":571445453,\"artistName\":\"Taylor Swift\",\"collectionName\":\"Red\",\"trackName\":\"I Knew You Were Trouble\",\"collectionCensoredName\":\"Red\",\"trackCensoredName\":\"I Knew You Were Trouble\",\"artistViewUrl\":\"https://itunes.apple.com/us/artist/taylor-swift/id159260351?uo=4\",\"collectionViewUrl\":\"https://itunes.apple.com/us/album/i-knew-you-were-trouble/id571445253?i=571445453&uo=4\",\"trackViewUrl\":\"https://itunes.apple.com/us/album/i-knew-you-were-trouble/id571445253?i=571445453&uo=4\",\"previewUrl\":\"http://a1689.phobos.apple.com/us/r1000/120/Music/v4/b3/b1/01/b3b101c5-0afd-a536-394d-962b7012e8e1/mzaf_2968645385761922805.aac.m4a\",\"artworkUrl30\":\"http://a4.mzstatic.com/us/r30/Music/v4/eb/bc/02/ebbc0219-a3a1-7c77-42ed-706251b98136/UMG_cvrart_00843930007134_01_RGB72_1200x1200_12UMDIM01007.30x30-50.jpg\",\"artworkUrl60\":\"http://a3.mzstatic.com/us/r30/Music/v4/eb/bc/02/ebbc0219-a3a1-7c77-42ed-706251b98136/UMG_cvrart_00843930007134_01_RGB72_1200x1200_12UMDIM01007.60x60-50.jpg\",\"artworkUrl100\":\"http://a2.mzstatic.com/us/r30/Music/v4/eb/bc/02/ebbc0219-a3a1-7c77-42ed-706251b98136/UMG_cvrart_00843930007134_01_RGB72_1200x1200_12UMDIM01007.100x100-75.jpg\",\"collectionPrice\":14.99,\"trackPrice\":1.29,\"releaseDate\":\"2012-10-22T07:00:00Z\",\"collectionExplicitness\":\"notExplicit\",\"trackExplicitness\":\"notExplicit\",\"discCount\":1,\"discNumber\":1,\"trackCount\":16,\"trackNumber\":4,\"trackTimeMillis\":219721,\"country\":\"USA\",\"currency\":\"USD\",\"primaryGenreName\":\"Country\",\"radioStationUrl\":\"https://itunes.apple.com/station/idra.571445453\"}','ecba2986c86e29910f198263af1185c2','2014-11-19 17:27:55','2014-11-19 17:27:55');
/*!40000 ALTER TABLE `songs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-20  2:48:12
