<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSongsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('songs', function(Blueprint $table)
		{
			$table->increments('song_id');
			$table->string('song_name', 300);
			$table->string('song_artist', 100)->nullable();
			$table->text('song_data');
			$table->string('song_hash', 32)->default('');
			$table->timestamps();

			$table->unique('song_hash', 'song_hash_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('songs');
	}

}
