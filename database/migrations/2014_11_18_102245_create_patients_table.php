<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePatientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('patients', function(Blueprint $table)
		{
			$table->increments('patient_id');
			$table->string('patient_name', 255);
			$table->string('patient_phone', 100);
			$table->smallInteger('patient_age')->unsigned();
			$table->integer('favorite_song_id')->unsigned()->nullable();
			$table->timestamps();

			$table->foreign('favorite_song_id')->references('song_id')->on('songs');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('patients');
	}

}
