<?php

use App\Patient;
use Illuminate\Database\Seeder;

class PatientsTableSeeder extends Seeder {

	public function run()
	{
		Patient::create([
			'patient_name' => 'Kamal Small',
			'patient_phone' => '532-498-9040',
			'patient_age' => 5
		]);
	}

}
