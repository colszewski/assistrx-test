## What I did and why ##
###Framework vs No Framework
 - For this coding test, I opted to use [Laravel 5.0](http://laravel.com/docs/master), a full blown PHP MVC framework. I chose to do this for a few reasons:
	 - I wanted to demonstrate a broader range of knowledge with PHP with subjects such as IoC and [PSR-4](http://www.php-fig.org/psr/psr-4/) namespacing.
	 - Frameworks force you to comply and adapt to a code base that you are not entirely familiar with at first, similar to starting at a new company.
	 - They remove common boilerplate code and allow you to focus on the more important aspects of a project.
 - However, I would like to emphasize that the majority of my daily programming is done outside of a framework.

###Database Schema
 - I tried to keep the included schema (arx_test.sql) as close to the original as possible. Laravel does add two extra timestamp columns, but other than that they are very similar.
 - Instead of using a MySQL trigger to calculate the song_data hash, I created a mutator of the attribute in the Song model and abstracted out the hashing function.
	 - This allows for easier checking of whether or not a song already exists in the database.
	 - It also makes it easier to change the hashing function in the future, if necessary.
 - The tables for this project can be created either by running the arx_test.sql commands or by using Laravel's [migration feature](http://laravel.com/docs/master/migrations#running-migrations).

###Bootstrap Styling
 - I also opted to use [Bootstrap](http://getbootstrap.com/) for this project as I am very familiar with it and it allows me to build much faster (and much prettier) than plain HTML.
 - In this case, I am using the default Bootstrap theme from a CDN.
 - In the real world, I would prefer to do some customization of Bootstrap's included variables so that it did not look exactly like the rest of the internet.

###Patient Listing Page
 - On this page I added a working JS name filter and sorted alphabetically by name.
 - Patients that have a song selected are also given a class to stand out more.
 - One tool I have used extensively in the past is [DataTables](http://datatables.net/) which adds features such as sorting, fixed headers, paging, and much more.
 - In an effort to include as little additional libraries as possible for this test project, I decided against including it here. I figured that I had already added a framework, Bootstrap, and Select2, so I didn't want to push my luck!

###Song Selection Page
 - This page is closely modeled after the original with a few enhancements to styling and functionality.
 - I included [Select2](http://ivaynberg.github.io/select2/) for its remote loading and templating capabilities.
 - I also added a preview of the selected song if one has been selected.
 - To clear a patient's selected song, click the 'x' to the right of the select box.

###Report Page
 - I decided to use a rainbow instead of a unicorn for the design because a rainbow looked better as a background. Using a unicorn would have presented additional challenges with placement.
 - I created two major report sections to include more detail.
#####Patient Report
 - The Patient Report shows only patients with a song selected by default because if a patient does not have a song selected, there is not much to show.
 - All patients can still be shown via a checkbox in the bottom right of the table footer.
 - Clicking the preview button shows the album art and allows users to preview the song. Showing the album art this way prevented the table rows from growing too large.
#####Song Report
 - In the Song Report section, users can see what the most popular artists, collections, tracks, and genres are.
 - By default this is limited to the top 5 entries, but this can be overridden with the parameter to _Song::getPopularityMetrics_.

## What I would do with more time and a larger budget ##
 - Remove the _Song Selection_ page and allow users to modify a patient's song directly from the _Patient Listing_ page. This would prevent having to go back-and-forth when changing songs for multiple patients.
 - Add more reports. From the initial project outline, adding a report to show the correlation of age and genre would be interesting.
 - Depending on the future scope of the project, using Angular for UI rather than the Blade templating engine.
	 - Modern PHP frameworks make creating a REST API very straight-forward.
	 - It would make editing patients inline even easier.
	 - Less page loads are usually a good thing!
	 - If the project was not going to get much bigger or be expanded, the conversion to Angular probably not make sense.
 - Use langauge files throughout the application.
	 - Could standardize error messages.
	 - Easier to modify a langauge file array than text hard-coded into PHP or templates.
 - As I said earlier, update the Bootstrap theme to match existing projects and adding DataTables for rendering large tables.