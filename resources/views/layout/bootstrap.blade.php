<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>ARX Test - {{ $title }}</title>
    <meta name="description" content="assistrx programming test">
    <meta name="author" content="assistrx-dw">

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/global.css">

    @yield('style')
</head>
<body class="{{ $background or null }}">
    <nav class="navbar navbar-default navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="top-nav">
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is('patient') ? 'active' : null }}">
                        <a href="/patient"><i class="glyphicon glyphicon-list-alt"></i> All Patients</a>
                    </li>
                    <li class="{{ Request::is('report') ? 'active' : null }}">
                        <a href="/report"><i class="glyphicon glyphicon-music"></i> Report</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        @yield('content')
    </div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

    @yield('script')
</body>
</html>
