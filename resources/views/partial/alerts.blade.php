@if (Session::has('error'))
    <div class="alert alert-warning">
        <i class="glyphicon glyphicon-warning-sign"></i> {{ Session::get('error') }}
    </div>
@endif

@if (Session::has('success'))
    <div class="alert alert-success">
        <i class="glyphicon glyphicon glyphicon-ok"></i> {{ Session::get('success') }}
    </div>
@endif
