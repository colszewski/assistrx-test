@extends('layout.bootstrap')

@section('style')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2-bootstrap.min.css">

    <style type="text/css">
        .bigdrop > .select2-results {
            max-height: 300px;
        }

        .select2-container.select2-dropdown-open {
            width: 400px;
          }
    </style>
@stop

@section('content')
    <div class="page-header">
        <h1>Song Selection</h1>
    </div>

    @include('partial.alerts')

    @if ($songData !== null)
        <h3>{{ $patient->patient_name }}'s currently selected song</h3>
        <div class="media">
            <a class="media-left" href="#">
                <img src="{{ $songData->artworkUrl100 }}">
            </a>
            <div class="media-body">
                <h4 class="media-heading">{{ $songData->trackName }}</h4>
                <i class="glyphicon glyphicon-user"></i> {{ $songData->artistName }}<br>
                <i class="glyphicon glyphicon-music"></i> {{ $songData->collectionName }}<br>
                <audio controls>
                    <source src="{{ $songData->previewUrl }}">
                </audio>
            </div>
        </div>
    @endif

    {!! Form::open(['action' => ['\App\Http\Controllers\PatientController@update', $patient->patient_id], 'method' => 'PUT']) !!}
        <h3>Assign a {{ ($songData !== null) ? 'new ' : null }}song to {{ $patient->patient_name }}</h3>
        <div class="form-group">
            <input type="hidden"
                   class="form-control input-sm"
                   name="rawSongData"
                   id="songSelector"
                   value="{{ ($songData !== null) ? json_encode($songData) : null }}">
        </div>
        <button type="submit" class="btn btn-success"><i class="glyphicon glyphicon-floppy-disk"></i> Save</button>
    {!! Form::close() !!}
@stop

@section('script')
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js"></script>
    <script>
        // Use Select2 for it's remote loading and templating capabilities
        $("#songSelector").select2({
            placeholder: "Search for a song",
            minimumInputLength: 1,
            dropdownCssClass: "bigdrop",
            allowClear: true,
            escapeMarkup: function (m) { return m; },
            id: function(data) { return JSON.stringify(data); },
            formatSelection: function(song) { return song.artistName + ' - ' + song.trackName; },
            ajax: {
                url: "https://itunes.apple.com/search",
                jsonpCallback : 'jsonCallback',
                dataType: 'jsonp',
                quietMillis: 250,
                data: function (term) {
                    return {
                        country: 'US',
                        term: term,
                        entity: 'song',
                        limit: 25
                    };
                },
                results: function (data) { return { results: data.results }; },
                cache: true
            },
            initSelection: function(element, callback) {
                var rawSongData = $(element).val();
                if (rawSongData !== "") {
                    var songData = JSON.parse(rawSongData);
                    callback(songData);
                }
            },
            formatResult: function(song) {
                return '<div class="media">' +
                    '<a class="media-left" href="#">' +
                       '<img src="' + song.artworkUrl100 + '">' +
                    '</a>' +
                    '<div class="media-body">' +
                       '<h4 class="media-heading">' + song.trackName + '</h4>' +
                        song.artistName + '<br>' +
                        song.collectionName +
                    '</div>' +
                '</div>';
            }
        });
    </script>
@stop
