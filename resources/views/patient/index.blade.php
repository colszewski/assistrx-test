@extends('layout.bootstrap')

@section('content')
    <div class="page-header">
        <h1>Patient Listing</h1>
    </div>

    @include('partial.alerts')

    <div class="form-group">
        <input id="nameFilter" class="form-control" placeholder="Filter by name">
    </div>

    <table id="patients" class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Phone</th>
                <th>Has Song</th>
                <th class="text-center">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($patients as $patient)
                <tr class="{{ $patient->hasFavoriteSong() ? 'success' : null }}">
                    <td class="patient-name">{{ $patient->patient_name }}</td>
                    <td class="patient-age">{{ $patient->patient_age }}</td>
                    <td class="patient-phone">{{ $patient->patient_phone }}</td>
                    <td class="patient-has-song">{{ $patient->favorite_song_id === null ? 'NO' : 'YES' }}</td>
                    <td class="patient-song text-center">
                        <a href="/patient/{{ $patient->patient_id }}/edit" class="btn btn-xs btn-primary">
                            <i class="glyphicon glyphicon-pencil"></i> Assign Song
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="5">{{ count($patients) }} patients found</td>
            </tr>
        </tfoot>
    </table>
@stop

@section('script')
    <script>
        var $patientRows = $('#patients').find('tbody > tr');

        // Case-insensitive patient name filter
        $('#nameFilter').on('input', function() {
            var filteredName = $(this).val().trim().toLowerCase();

            $patientRows.each(function(idx, row) {
                var $row = $(row);

                $row.show();

                var rowPatientName = $row.find('.patient-name').text().toLowerCase();
                if(rowPatientName.indexOf(filteredName) === -1) {
                    $row.hide();
                }
            });
        });
    </script>
@stop
