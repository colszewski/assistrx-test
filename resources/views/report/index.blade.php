@extends('layout.bootstrap')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="page-header">
                <h1>Patient Report</h1>
            </div>

            <table id="patients" class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th colspan="2">Patient</th>
                        <th colspan="5">Favorite Song</th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Artist</th>
                        <th>Album</th>
                        <th>Name</th>
                        <th>Genre</th>
                        <th class="text-center">Preview</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($patients as $patient)
                        <tr>
                            <td>{{ $patient->patient_name }}</td>
                            <td>{{ $patient->patient_age }}</td>

                            @if ($patient->favoriteSong === null)
                                <td colspan="5" class="warning no-song-selected"><em>No song selected</em></td>
                            @else
                                <?php $songData = json_decode($patient->favoriteSong->song_data); ?>
                                <td>{{ $songData->artistName }}</td>
                                <td>{{ $songData->collectionName }}</td>
                                <td>{{ $songData->trackName }}</td>
                                <td>{!! $songData->primaryGenreName or '<em>N/A</em>' !!}</td>
                                <td class="text-center">
                                    <button type="button"
                                            class="btn btn-xs btn-primary songPreview"
                                            data-preview-url="{{ $songData->previewUrl }}"
                                            data-artwork-url="{{ $songData->artworkUrl100 }}">
                                        <i class="glyphicon glyphicon-headphones"></i>
                                    </button>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">{{ count($patients) }} patients found</td>
                        <td colspan="4" class="text-right">
                            <div class="checkbox no-margin">
                                <label>
                                    <input type="checkbox" id="showAllPatients"> Show all patients
                                </label>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>


        <div class="panel-body">
            <div class="page-header">
                <h1>Song Report</h1>
            </div>

            <div class="row">
                @foreach ($songMetrics as $key => $songMetric)
                    <div class="col-lg-3">
                        <div class="panel panel-default">
                            <div class="panel-heading"><strong>Most Popular {{ ucfirst($key) . 's' }}</strong></div>
                            <table class="table table-striped table-condensed">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{ ucfirst($key) }}</th>
                                        <th>Patients</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($songMetric as $idx => $metric)
                                    <tr>
                                        <td>{{ $idx + 1 }}</td>
                                        <td>{{ $metric['name'] }}</td>
                                        <td>{{ $metric['count'] }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

@stop

@section('script')
    <script>
        var $patients = $('#patients'),
            $patientRows = $patients.find('tbody > tr');

        // Show album art and a song preview in a click-activated popover for patients with a favorite song
        $patients.popover({
            selector: '.songPreview',
            html: true,
            content: function() {
                return '<div>' +
                        '<img src="' + $(this).data('artwork-url') + '" class="img-responsive">' +
                    '</div>' +
                    '<audio controls class="short-audio">' +
                        '<source src="' + $(this).data('preview-url') + '">' +
                    '</audio>';
            }
        });

        // Allow users to toggle between showing all users and just users with a favorite song
        // By default, only patients with a song are shown
        $('#showAllPatients').on('change', function() {
            var isChecked = $(this).prop('checked');

            if(isChecked) {
                $patientRows.show();
                return;
            }

            $patientRows.each(function(idx, row) {
                var $row = $(row);

                $row.show();

                if($row.children('.no-song-selected').length === 1) {
                    $row.hide();
                }
            });
        }).trigger('change');
    </script>
@stop
